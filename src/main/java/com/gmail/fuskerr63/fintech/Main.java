package com.gmail.fuskerr63.fintech;

import com.gmail.fuskerr63.fintech.domain.Student;
import com.gmail.fuskerr63.fintech.stream.StreamResolver;
import com.gmail.fuskerr63.fintech.stream.StudentStreamResolver;

import java.util.List;
import java.util.Map;
import java.util.Set;

public class Main {
    private static final List<Student> studentList = List.of(
            new Student("8de25695", "Stella", 18),
            new Student("ee1056ac", "Pollyanna", 19),
            new Student("13d5f40b", "Jason", 20),
            new Student("ff1a02ca", "Stella", 22),
            new Student("3b0bce52", "May", 19),
            new Student("4066454d", "May", 18),
            new Student("047f10f8", "Stella", 20),
            new Student("be6bbee3", "May", 21),
            new Student("0099be30", "Jason", 22),
            new Student("9d422634", "Jason", 20)
    );
    private static final String STUDENT_NAME_TEST = "Jason";
    private static final int STUDENT_AGE_TEST = 25;

    public static void main(String[] args) {
        StreamResolver<Student> streamResolver = new StudentStreamResolver(studentList);

        // суммарный возраст для определенного имени
        int totalAge = streamResolver.totalAgeForName(STUDENT_NAME_TEST);
        System.out.println("Total age: " + totalAge);

        // Set, который содержит в себе только имена учеников
        Set<String> studentNames = streamResolver.getSetNames();
        System.out.println("Set with names: " + studentNames);

        // узнать, содержит ли список хотя бы одного ученика, у которой возраст больше заданного числа
        boolean containsStudent = streamResolver.containsGreaterAge(STUDENT_AGE_TEST);
        System.out.println("Contains student: " + containsStudent);

        // преобразовать список в Map, у которой ключ - уникальный идентификатор, значение - имя
        Map<String, String> mapIdToName = streamResolver.getMapIdToName();
        System.out.println("Map(id -> name): " + mapIdToName);

        // преобразовать список в Map, у которой ключ - возраст, значение - коллекция объектов Student,
        // которым соответствует возраст, указанный в ключе
        Map<Integer, List<Student>> mapAgeToList = streamResolver.getMapAgeToList();
        System.out.println("Map(age -> List): " + mapAgeToList);
    }
}