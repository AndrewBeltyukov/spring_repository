package com.gmail.fuskerr63.fintech.stream;

import java.util.List;
import java.util.Map;
import java.util.Set;

public interface StreamResolver<T> {
    int totalAgeForName(String name);

    Set<String> getSetNames();

    boolean containsGreaterAge(int age);

    Map<String, String> getMapIdToName();

    Map<Integer, List<T>> getMapAgeToList();
}
