package com.gmail.fuskerr63.fintech.stream;

import com.gmail.fuskerr63.fintech.domain.Student;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

public class StudentStreamResolver implements StreamResolver<Student> {
    private final List<Student> studentList;

    public StudentStreamResolver(List<Student> studentList) {
        this.studentList = studentList;
    }

    @Override
    public int totalAgeForName(String name) {
        return studentList.stream()
                .filter(student -> student.getName().equals(name))
                .mapToInt(Student::getAge)
                .sum();
    }

    @Override
    public Set<String> getSetNames() {
        return studentList.stream()
                .map(Student::getName)
                .collect(Collectors.toSet());
    }

    @Override
    public boolean containsGreaterAge(int age) {
        return studentList.stream()
                .anyMatch(student -> student.getAge() > age);
    }

    @Override
    public Map<String, String> getMapIdToName() {
        return studentList.stream()
                .collect(Collectors.toMap(Student::getId, Student::getName));
    }

    @Override
    public Map<Integer, List<Student>> getMapAgeToList() {
        return studentList.stream()
                .collect(Collectors.groupingBy(Student::getAge));
    }
}
